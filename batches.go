/*
Package batches provides a simple method
for sequentially processing batches of records.
It is mostly intended for interacting with databases
where records are often best processed one-at-a-time
but best loaded in large groups.

See the example for direct usage,
but it is often more convenient to use the
bitbucket.org/classroomsystems/querybatches package.
*/
package batches

import (
	"fmt"
	"reflect"
)

// AnyErr checks whether any of the seqs has an error
// set, returning the first one it finds or nil if
// there is none.
func AnyErr(seqs ...*Seq) error {
	for i := range seqs {
		if seqs[i].err != nil {
			return seqs[i].err
		}
	}
	return nil
}

// AllDone checks whether all the seqs are done,
// returning true if so.
func AllDone(seqs ...*Seq) bool {
	for i := range seqs {
		if !seqs[i].done {
			return false
		}
	}
	return true
}

// A Loader loads a batch of records into dst,
// which is a pointer to a zero-length slice
// of the type passed to Seq.Next.
// The dst value will be reused
// by subsequent calls of LoadBatch.
//
// When there are no records to load,
// LoadBatch should return nil
// without appending any record to dst.
// The Seq will be marked Done.
// If LoadBatch returns an error,
// the Seq will be marked Done
// and the error returned by Seq.Err.
// In either case, once the Seq is marked Done,
// LoadBatch will not be called again.
type Loader interface {
	LoadBatch(dst interface{}) error
}

// A LoadCloser is a Loader
// that should be closed
// when no longer needed.
// See Seq's Close method.
type LoadCloser interface {
	Loader
	Close() error
}

type nilLoader struct{}

func (nilLoader) LoadBatch(dst interface{}) error {
	return nil
}

// A Seq enables reading records one-at-a-time
// from a Loader which loads them in large batches.
// A Seq is not safe for concurrent use
// from multiple goroutines.
type Seq struct {
	loader    Loader
	dst       reflect.Value
	dstSlice  reflect.Value
	doneValue reflect.Value
	n         int
	done      bool
	err       error
}

// New returns a new Seq reading from the given Loader.
func New(loader Loader) *Seq {
	return &Seq{loader: loader}
}

// NewWithDst is like new but gives the caller control
// over allocating the slice used to hold record batches.
// The dst argument must be a slice or pointer to a
// slice of the type to be used in calls to Next. If the
// slice has contents, those will be iterated through
// before calling on the Loader.
func NewWithDst(loader Loader, dst interface{}) *Seq {
	s := New(loader)
	s.dst = reflect.ValueOf(dst)
	if s.dst.Kind() == reflect.Slice {
		sl := s.dst
		s.dst = reflect.New(reflect.SliceOf(sl.Type().Elem()))
		s.dst.Elem().Set(sl)
	}
	if s.dst.Kind() != reflect.Ptr || s.dst.Elem().Kind() != reflect.Slice {
		panic(fmt.Errorf("batches: bad type for destination slice: %T", dst))
	}
	s.dstSlice = s.dst.Elem()
	return s
}

// NewFromSlice creates a new Seq
// to iterate over the elements of slice,
// which must be a slice or a pointer to a slice
// of the type to be used in calls to Next.
func NewFromSlice(slice interface{}) *Seq {
	return NewWithDst(nilLoader{}, slice)
}

// NewWithErr returns a new Seq that will load no records,
// is already done, and will return err from its Err method.
//
// This can be useful to report errors from functions that create Seqs.
func NewWithErr(err error) *Seq {
	return &Seq{loader: nilLoader{}, done: true, err: err}
}

// Loader returns the Loader used by s.
func (s *Seq) Loader() Loader {
	return s.loader
}

// Next sets dst to the next value in the sequence.
// Dst must be a pointer to a type
// supported by the Seq's Loader.
// The same type must be used
// in every call to Next for a given Seq.
//
// If there has been an error
// or there are no more values in the sequence,
// dst will be set to the Seq's DoneValue.
// If the Seq does not have a DoneValue,
// dst will be left alone.
func (s *Seq) Next(dst interface{}) {
	if s.done && !s.doneValue.IsValid() {
		return
	}
	dv := reflect.ValueOf(dst)
	if dv.IsValid() && dv.Kind() == reflect.Ptr && !dv.IsNil() {
		dv = dv.Elem()
	}
	if !dv.IsValid() || !dv.CanSet() {
		panic(fmt.Errorf("batches: Seq.Next called with unsettable value %v", dst))
	}
	if s.done {
		dv.Set(s.doneValue)
	}
	if !s.dst.IsValid() {
		s.dst = reflect.New(reflect.SliceOf(dv.Type()))
		s.dstSlice = s.dst.Elem()
	}
	if s.n >= s.dstSlice.Len() {
		s.n = 0
		s.dstSlice.Set(s.dstSlice.Slice(0, 0))
		s.err = s.loader.LoadBatch(s.dst.Interface())
		if s.err != nil || s.dstSlice.Len() == 0 {
			s.done = true
			if s.doneValue.IsValid() {
				dv.Set(s.doneValue)
			}
			return
		}
	}
	dv.Set(s.dstSlice.Index(s.n))
	s.n++
}

// SetDoneValue sets the value that Next will use
// when there are no more values in the sequence.
// Sequences have no DoneValue by default.
func (s *Seq) SetDoneValue(v interface{}) {
	s.doneValue = reflect.ValueOf(v)
}

// Done returns true if there are no more values in the sequence
// or if there has been an error.
func (s *Seq) Done() bool {
	return s.done
}

// Err returns the first error encountered
// while processing the sequence
// or nil if there has been no error.
func (s *Seq) Err() error {
	return s.err
}

// Close calls the Loader's Close method
// if it has one, returning any error.
// If the Loader has no Close method,
// Close simply returns nil.
func (s *Seq) Close() error {
	c, ok := s.loader.(LoadCloser)
	if !ok {
		return nil
	}
	return c.Close()
}
