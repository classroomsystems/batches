package batches_test

import (
	"database/sql"
	"log"
	"math/rand"
)

func init() {
	var err error
	DB, err = sql.Open("sqlite3", ":memory:")
	if err != nil {
		log.Fatalln(err)
	}
	tx, err := DB.Begin()
	if err != nil {
		log.Fatalln(err)
	}
	defer tx.Commit()
	x := &errExec{x: tx}
	x.Exec("create table t1 (ID integer primary key, Value1 integer)")
	x.Exec("create table t2 (ID integer primary key, Value2 integer)")
	rand.Seed(0) // The test needs to be reproducible.
	for id := 1; id < 30000 && x.err == nil; id++ {
		switch rand.Intn(4) {
		case 0:
			// no record
		case 1:
			x.Exec("insert into t1 (ID, Value1) values (?, ?)", id, rand.Int31())
		case 2:
			x.Exec("insert into t2 (ID, Value2) values (?, ?)", id, rand.Int31())
		case 3:
			x.Exec("insert into t1 (ID, Value1) values (?, ?)", id, rand.Int31())
			x.Exec("insert into t2 (ID, Value2) values (?, ?)", id, rand.Int31())
		}
	}
	if x.err != nil {
		log.Fatalln(err)
	}
}

type errExec struct {
	x interface {
		Exec(query string, args ...interface{}) (sql.Result, error)
	}
	err error
}

func (e *errExec) Exec(query string, args ...interface{}) {
	if e.err != nil {
		return
	}
	_, e.err = e.x.Exec(query, args...)
}

const maxInt64 = 9223372036854775807

func min(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}
