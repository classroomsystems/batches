package batches_test

import (
	"database/sql"
	"fmt"
	"log"

	"bitbucket.org/classroomsystems/batches"

	_ "github.com/mattn/go-sqlite3"
)

var DB *sql.DB

type record1 struct {
	ID     int64
	Value1 int64
}

type record2 struct {
	ID     int64
	Value2 int64
}

// The batches package can help process records from
// a database. The processing code can be written as
// if records are loaded one-at-a-time from any number
// of queries, but the records are actually loaded in
// batches of a specified size, balancing memory
// requirements and database latency.
//
// In the example below, we collate records from two
// tables that share an ID space. Note that only one
// query is active at a time, even though processing
// is interleaved, so this can be done on a single
// database connection or within a transaction.
func Example_batchSQL() {
	var inBoth, onlyInT1, onlyInT2 int
	t1 := batches.NewWithDst(&t1Batches{}, make([]record1, 0, 10000))
	t1.SetDoneValue(record1{ID: maxInt64})
	t2 := batches.NewWithDst(&t2Batches{}, make([]record2, 0, 10000))
	t2.SetDoneValue(record2{ID: maxInt64})
	var r1 record1
	var r2 record2
	t1.Next(&r1)
	t2.Next(&r2)
	for !t1.Done() || !t2.Done() {
		id := min(r1.ID, r2.ID)
		if id == r1.ID && id == r2.ID {
			// do some processing
			inBoth++
			t1.Next(&r1)
			t2.Next(&r2)
		} else if id == r1.ID {
			// do some processing
			onlyInT1++
			t1.Next(&r1)
		} else {
			// do some processing
			onlyInT2++
			t2.Next(&r2)
		}
	}
	if err := batches.AnyErr(t1, t2); err != nil {
		log.Fatalln(err)
	}
	fmt.Println("record counts:", onlyInT1, inBoth, onlyInT2)
	// Output:
	// loaded 10000 records from t1
	// loaded 10000 records from t2
	// loaded 5079 records from t1
	// loaded 4913 records from t2
	// loaded 0 records from t2
	// loaded 0 records from t1
	// record counts: 7580 7499 7414
}

type t1Batches record1

func (r *t1Batches) LoadBatch(dst interface{}) error {
	d, ok := dst.(*[]record1)
	if !ok {
		return fmt.Errorf("bad dst type for t1Batches: %T", dst)
	}
	rs, err := DB.Query("select ID, Value1 from t1 where ID > ? order by ID limit ?", r.ID, cap(*d))
	if err != nil {
		return err
	}
	defer rs.Close()
	for rs.Next() {
		err = rs.Scan(&r.ID, &r.Value1)
		if err != nil {
			return err
		}
		*d = append(*d, record1(*r))
	}
	fmt.Println("loaded", len(*d), "records from t1")
	return rs.Err()
}

type t2Batches record2

func (r *t2Batches) LoadBatch(dst interface{}) error {
	d, ok := dst.(*[]record2)
	if !ok {
		return fmt.Errorf("bad dst type for t2Batches: %T", dst)
	}
	rs, err := DB.Query("select ID, Value2 from t2 where ID > ? order by ID limit ?", r.ID, cap(*d))
	if err != nil {
		return err
	}
	defer rs.Close()
	for rs.Next() {
		err = rs.Scan(&r.ID, &r.Value2)
		if err != nil {
			return err
		}
		*d = append(*d, record2(*r))
	}
	fmt.Println("loaded", len(*d), "records from t2")
	return rs.Err()
}
