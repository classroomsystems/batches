package batches

import (
	"errors"
	"testing"
)

type countLoader struct {
	max int
	n   int
}

func (l *countLoader) LoadBatch(di interface{}) error {
	dst := di.(*[]int)
	for i := 0; i < 1000 && l.n <= l.max; i, l.n = i+1, l.n+1 {
		*dst = append(*dst, l.n)
	}
	return nil
}

func TestSeq(t *testing.T) {
	seq := New(&countLoader{max: 1000000})
	var iSeq, iCount, errs int
	var err error
	seq.SetDoneValue(-1)
	seq.Next(&iSeq)
	for !seq.Done() {
		if errs > 10 {
			t.Fatal("too many errors")
		}
		if iSeq != iCount {
			t.Errorf("bad sequence %d != %d", iSeq, iCount)
			errs++
		}
		iCount++
		seq.Next(&iSeq)
	}
	if iSeq != -1 {
		t.Errorf("unexpected done value, got %d expecting %d", iSeq, -1)
	}
	// Be sure we get the done value again if we call Next again.
	iSeq = 0
	seq.Next(&iSeq)
	if iSeq != -1 {
		t.Errorf("unexpected second done value, got %d expecting %d", iSeq, -1)
	}
	if err = seq.Err(); err != nil {
		t.Error("unexpected error:", err)
	}
	if err = seq.Close(); err != nil {
		t.Error("unexpected close error:", err)
	}
}

var errTest = errors.New("errTest")

type closeLoader bool

func (l *closeLoader) LoadBatch(_ interface{}) error {
	return nil
}
func (l *closeLoader) Close() error {
	*l = true
	return errTest
}

func TestClose(t *testing.T) {
	l := new(closeLoader)
	s1 := New(l)
	s2 := New(nilLoader{})
	err := s1.Close()
	if err != errTest {
		t.Error("wrong error, got", err, "expecting errTest")
	}
	if !*l {
		t.Error("not closed")
	}
	err = s2.Close()
	if err != nil {
		t.Error("unexpected error:", err)
	}
}

type errLoader struct{}

func (errLoader) LoadBatch(_ interface{}) error {
	return errTest
}

func TestErrors(t *testing.T) {
	s1 := NewFromSlice([]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
	s2 := New(errLoader{})
	var i int
	err := AnyErr(s1, s2)
	if err != nil {
		t.Fatal("early error:", err)
	}
	for !AllDone(s1, s2) {
		s1.Next(&i)
		s2.Next(&i)
	}
	if i != 10 {
		t.Error("didn't finish the sequence:", i)
	}
	if err = AnyErr(s1, s2); err != errTest {
		t.Error("wrong error, got", err, "expecting errTest")
	}
}

func TestPanic(t *testing.T) {
	var (
		si = []int{1, 2, 3, 4}
		i  int
		f  float64
	)
	var cases = []struct {
		slice       interface{}
		dst         interface{}
		shouldPanic bool
	}{
		{si, si, true}, {si, &si, true},
		{si, i, true}, {si, &i, false},
		{si, f, true}, {si, &f, true},
		{i, &i, true},
	}
	for _, c := range cases {
		var panicVal interface{}
		func() {
			defer func() {
				panicVal = recover()
			}()
			seq := NewFromSlice(c.slice)
			seq.Next(c.dst)
		}()
		if c.shouldPanic && panicVal == nil {
			t.Errorf("TestPanic(%T, %T) should have panicked", c.slice, c.dst)
		}
		if !c.shouldPanic && panicVal != nil {
			t.Errorf("TestPanic(%T, %T) unexpected panic: %v", c.slice, c.dst, panicVal)
		}
	}
}
